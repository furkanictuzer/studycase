using UnityEngine;

namespace _04_RunAwayFromTheCops
{
    public class CollectableObjectOnPath : MonoBehaviour
    {
        private const float AnimTime = 0.5f;
        
        public void CollectAnim(Transform parent)
        {
            transform.parent = parent;
            transform.LeanScale(Vector3.zero, AnimTime).setEaseInOutBack().setOnComplete(() => gameObject.SetActive(false));
        }
    }
}
