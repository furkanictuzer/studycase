using System;
using UnityEngine;

namespace _04_RunAwayFromTheCops
{
    public class RunForward : MonoBehaviour
    {
        [SerializeField] private float speed;

        private bool _firstTouch;
        private bool _isFinish = false;


        private void Awake()
        {
            EventManager.Instance.AddMethodFinish(Finish);
        }

        private void Update()
        {
            switch (_firstTouch)
            {
                case false when Input.GetMouseButtonDown(0):
                    _firstTouch = true;
                
                    AnimatorController.Instance.SetRun(true);
                    break;
                case true when !_isFinish:
                    Run();
                    break;
            }
        }

        private void Finish()
        {
            _isFinish = true;
        }

        private void Run()
        {
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
        }
        
        
    }
}
