using System;
using UnityEngine;

namespace _04_RunAwayFromTheCops
{
    public class CopCanvasPositioner : MonoBehaviour
    {
        [SerializeField] private GameObject copCanvas;

        private void FixedUpdate()
        {
            RotateForCam();
        }

        private void RotateForCam()
        {
            var pos = transform.position + Vector3.forward;
            
            pos.y = copCanvas.transform.position.y;
            
            copCanvas.transform.LookAt(pos);
        }
    }
}
