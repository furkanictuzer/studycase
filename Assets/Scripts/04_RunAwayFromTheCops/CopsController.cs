using System;
using UnityEngine;

namespace _04_RunAwayFromTheCops
{
    public class CopsController : MonoBehaviour
    {
        [SerializeField] private Transform playerTransform;

        [SerializeField] private float speed;

        private CopsAnimatorController _animatorController;
        
        private bool _follow = false;
        private bool _run = false;

        private void Awake()
        {
            _animatorController = GetComponent<CopsAnimatorController>();
            
            EventManager.Instance.AddMethodFinish(Finish);
        }

        private void Update()
        {
            if (ControlPlayer() && !_follow)
            {
                _follow = true;
            }
            else switch (_follow)
            {
                case true when !_run:
                    _animatorController.SetRun(true);
                    _run = true;
                    break;
                case true:
                    FollowTarget();
                    break;
            }
        }

        private void Finish()
        {
            _follow = false;
        }

        private void FollowTarget()
        {
            transform.position = Vector3.Lerp(transform.position, playerTransform.position - Vector3.back * 3,
                Time.deltaTime * speed);
            
            transform.LookAt(playerTransform);
        }

        private bool ControlPlayer()
        {
            var ray01 = new Ray(transform.position, Vector3.right);
            var ray02 = new Ray(transform.position, Vector3.left);

            RaycastHit hitInfo01;
            RaycastHit hitInfo02;
            
            if (Physics.Raycast(ray01,out hitInfo01,10))
            {
                return hitInfo01.collider.gameObject.GetComponent<CharacterCollisionController>();
            }

            return Physics.Raycast(ray02,out hitInfo02,10) ? hitInfo02.collider.gameObject.GetComponent<CharacterCollisionController>() : false;
        }
    }
}
