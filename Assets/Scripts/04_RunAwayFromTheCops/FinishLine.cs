using System;
using UnityEngine;

namespace _04_RunAwayFromTheCops
{
    public class FinishLine : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<CharacterCollisionController>())
            {
                EventManager.Instance.OnAction(ActionType.Finish);
            }
        }
    }
}
