using System;
using UnityEngine;

namespace _04_RunAwayFromTheCops
{
    public class CopsAnimatorController : MonoBehaviour
    {
        private Animator _animator;
        private static readonly int Run = Animator.StringToHash("Run");

        private void Awake()
        {
            _animator = GetComponent<Animator>();

            EventManager.Instance.AddMethodFinish(Finish);
        }

        private void Finish()
        {
            SetRun(false);
        }
        
        public void SetRun(bool run)
        {
            _animator.SetBool(Run, run);
        }
    }
}
