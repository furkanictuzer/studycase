using System;
using UnityEngine;

namespace _04_RunAwayFromTheCops
{
    public class CharacterCollisionController : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<CollectableObjectOnPath>())
            {
                var obj = other.GetComponent<CollectableObjectOnPath>();
                
                obj.CollectAnim(transform);
            }
        }
    }
}
