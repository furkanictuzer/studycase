using System.Collections;
using StealObjects;
using UnityEngine;

namespace _02_StealObjects
{
    public class CollectableObject : MonoBehaviour
    {
        private CollectController _collectController;

        private const float AnimTime = .7f;

        private void Awake()
        {
            _collectController = transform.parent.GetComponent<CollectController>();
        }

        private void OnMouseDown()
        {
            CollectAnim();
        }

        private void CollectAnim()
        {
            StartCoroutine(CollectAnimCoroutine());
            
        }

        private IEnumerator CollectAnimCoroutine()
        {
            _collectController.CollectObject(this);
            transform.LeanScale(Vector3.zero, AnimTime).setEaseInOutBack();

            yield return new WaitForSeconds(AnimTime * 0.5f);

            _collectController.CollectParticle(transform);
        }
    }
}
