using System;
using System.Collections.Generic;
using _02_StealObjects;
using Unity.Mathematics;
using UnityEngine;

namespace StealObjects
{
    public class CollectController : MonoBehaviour
    {
        [SerializeField] private List<CollectableObject> _collectableObjects = new List<CollectableObject>();
        
        [SerializeField] private ParticleSystem collectParticlePrefab;

        private int _initObjNum;

        private void Awake()
        {
            for (var i = 0; i < transform.childCount; i++)
            {
                _collectableObjects.Add(transform.GetChild(i).GetComponent<CollectableObject>());
            }

            _initObjNum = _collectableObjects.Count;
        }

        private void ControlObjects()
        {
            if (_collectableObjects.Count == 0)
                EventManager.Instance.OnAction(ActionType.Finish);
        }

        public void CollectObject(CollectableObject obj)
        {
            _collectableObjects.Remove(obj);
            
            var amount = 1 - (float)_collectableObjects.Count / (float)_initObjNum;
            
            ProgressBarController.Instance.SetBar(amount);
            
            ControlObjects();
        }

        public void CollectParticle(Transform obj)
        {
            Instantiate(collectParticlePrefab, obj.position, Quaternion.identity);
        }
    }
}
