using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeController : MonoBehaviour
{
    [SerializeField] private float xClamp;
    
    private float _firstValue;
    private float _currentValue;

    private float _lastSumValue;
    private float _screenWidth;

    private bool _handsUp;
    
    private bool _touchable = true;

    private const float Sensitivity = 10f;
    private float _sumValue;


    private void Awake()
    {
        EventManager.Instance.AddMethodFinish(DisableSwipe);
    }

    private void Start()
    {
        _screenWidth = Screen.width;
    }

    private void Update()
    {
        if (!_touchable) return;
        
        Swipe();
        Move(_sumValue);

    }

    private void Swipe()
    {
        if (Input.GetMouseButton(0))
        {
            if (_handsUp)
            {
                _firstValue = Input.mousePosition.x;
                _handsUp = false;
            }

            _currentValue = Input.mousePosition.x;

            _sumValue = _currentValue - _firstValue;
            
            _sumValue /= _screenWidth;

            _sumValue *= Sensitivity;
            _sumValue += _lastSumValue;
        }
        else
        {
            _lastSumValue = _sumValue;
            _handsUp = true;
        }

        if (_sumValue > xClamp)
        {
            _sumValue = xClamp;
            _lastSumValue = xClamp;
            _handsUp = true;
        }else if (_sumValue < -xClamp)
        {
            _sumValue = -xClamp;
            _lastSumValue = -xClamp;
            _handsUp = true;
        }
    }

    private void Move(float value)
    {
        var localPosition = transform.localPosition;
        localPosition = Vector3.Lerp(
            localPosition, 
            new Vector3(value, localPosition.y, localPosition.z),
            10f * Time.deltaTime);
        
        transform.localPosition = localPosition;
    }

    private void DisableSwipe()
    {
        _touchable = false;
    }

}
