using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ActionType
{
    Finish,
    Fail
}
public class EventManager : MonoSingleton<EventManager>
{
    private Action _finishAction; 
    private Action _failAction;
    
    public bool isComplete;

    public void OnAction(ActionType actionType)
    {
        switch (actionType)
        {
            case ActionType.Finish:
                _finishAction?.Invoke();
                break;
            case ActionType.Fail:
                _failAction?.Invoke();
                break;
        }
    }

    public void AddMethodFail(params Action[] methods)
    {
        foreach (var method in methods)
        {
            _failAction += method;
        } 
    }
    
    public void AddMethodFinish(params Action[] methods)
    {
        foreach (var method in methods)
        {
            _finishAction += method;
        } 
    }
    
}
