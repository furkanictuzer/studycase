using System;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBarController : MonoSingleton<ProgressBarController>
{
    [SerializeField] private Image progressBar;

    private float _targetAmount = 0f;

    private void FixedUpdate()
    {
        if (Math.Abs(_targetAmount - progressBar.fillAmount) > 0.01f)
        {
            progressBar.fillAmount = Mathf.Lerp(progressBar.fillAmount, _targetAmount, 0.1f);
        }
    }

    public void SetBar(float amount)
    {
        _targetAmount = amount;
    }
}