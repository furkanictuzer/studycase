using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    [SerializeField] private Canvas finishCanvas;
    [SerializeField] private Canvas failCanvas;

    private void Awake()
    {
        StartCoroutine(ActivateCanvas(false, 0, finishCanvas, failCanvas));
        
        EventManager.Instance.AddMethodFinish(Finish);
        EventManager.Instance.AddMethodFail(Fail);
    }

    private void Finish()
    {
        StartCoroutine(ActivateCanvas( true, 1,finishCanvas));
    }
    
    private void Fail()
    {
        StartCoroutine(ActivateCanvas( true, 1,failCanvas));
    }
    
    
    
    private IEnumerator ActivateCanvas(bool activate, float timeInSec,params Canvas[] canvas)
    {
        yield return new WaitForSeconds(timeInSec);
        
        foreach (var var in canvas)
            var.enabled = activate;
        
    }
}
