using System;
using UnityEngine;

namespace _03_CleanBeach
{
    public class Garbage : MonoBehaviour
    {
        private GarbageController _garbageController;

        private void Awake()
        {
            _garbageController = transform.parent.GetComponent<GarbageController>();
        }

        public void CollectThis()
        {
            _garbageController.CollectGarbage(this);
        }
    }
}
