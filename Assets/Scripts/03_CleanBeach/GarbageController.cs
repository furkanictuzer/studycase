using System;
using System.Collections.Generic;
using UnityEngine;

namespace _03_CleanBeach
{
    public class GarbageController : MonoBehaviour
    {
        private List<Garbage> _garbage = new List<Garbage>();

        private void Awake()
        {
            for (var i = 0; i < transform.childCount; i++)
            {
                _garbage.Add(transform.GetChild(i).GetComponent<Garbage>());
            }
        }

        private void ControlGarbage()
        {
            if (_garbage.Count == 0)
            {
                EventManager.Instance.OnAction(ActionType.Finish);
            }
        }

        public void CollectGarbage(Garbage garbage)
        {
            _garbage.Remove(garbage);
            
            ControlGarbage();
        }
    }
}
