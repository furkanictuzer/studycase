using System;
using DG.Tweening;
using UnityEngine;

namespace _03_CleanBeach
{
    public class BagCollisionController : MonoBehaviour
    {
        [SerializeField] private Transform bagCenterPos;

        private const float CollectAnimTimeInSec = .5f;

        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<Garbage>())
            {
                var obj = other.GetComponent<Garbage>();
                
                obj.CollectThis();
                
                CollectAnim(other.transform);
            }
        }

        private void CollectAnim(Transform obj)
        {
            obj.parent = transform.parent;
            
            obj.LeanMoveLocal(bagCenterPos.localPosition, CollectAnimTimeInSec)
                .setOnComplete(() => obj.gameObject.SetActive(false));
        }
    }
}
