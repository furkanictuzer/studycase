using UnityEngine;

namespace _01_CleanCarpet
{
    public class FollowTargetInScreen : MonoBehaviour
    {
        [SerializeField] private Transform target;

        private void FixedUpdate()
        {
            transform.position = Camera.main.WorldToScreenPoint(target.position);
        }
    }
}
