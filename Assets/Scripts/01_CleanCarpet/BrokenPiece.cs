using UnityEngine;

namespace _01_CleanCarpet
{
    public class BrokenPiece : MonoBehaviour
    {
        [SerializeField] private PieceController pieceController;
        
        private Vector3 _maxXZ = new Vector3(5, 2, 5), _minXZ = new Vector3(-5, 0, -5);

        public bool isCollected;

        private void Awake()
        {
            pieceController = transform.parent.GetComponent<PieceController>();
        }

        private void Update()
        {
            if (!isCollected)
            {
                transform.position = ClampedTransform();
            }
            
        }

        private Vector3 ClampedTransform()
        {
            var pos = transform.position;
            
            pos.x = Mathf.Clamp(pos.x, _minXZ.x, _maxXZ.x);
            pos.y = Mathf.Clamp(pos.y, _minXZ.y, _maxXZ.y);
            pos.z = Mathf.Clamp(pos.z, _minXZ.z, _maxXZ.z);

            return pos;
        }

        public void CollectPiece()
        {
            pieceController.CollectPiece(this);
        }
    }
}
