using UnityEngine;

namespace _01_CleanCarpet
{
    public class GatheringAreaController : MonoBehaviour
    {
        [SerializeField] private Transform areaCenter;

        private void OnTriggerEnter(Collider other)
        {
            if (!other.GetComponent<BrokenPiece>()) return;
            
            var obj = other.GetComponent<BrokenPiece>();

            if (obj.isCollected) return;
                
            obj.isCollected = true;

            other.transform.LeanMove(areaCenter.position, 1).setOnComplete(() => obj.CollectPiece());
        }
    }
}
