using System.Collections.Generic;
using UnityEngine;

namespace _01_CleanCarpet
{
    public class PieceController : MonoBehaviour
    {
        private List<BrokenPiece> _brokenPieces = new List<BrokenPiece>();

        private int _initObjNum;
        private void Awake()
        {
            for (var i = 0; i < transform.childCount; i++)
            {
                _brokenPieces.Add(transform.GetChild(i).GetComponent<BrokenPiece>());
            }
            
            _initObjNum = _brokenPieces.Count;
        }

        private void ControlPieces()
        {
            if (_brokenPieces.Count == 0)
            {
                EventManager.Instance.OnAction(ActionType.Finish);
            }
        }

        public void CollectPiece(BrokenPiece piece)
        {
            _brokenPieces.Remove(piece);
            
            var amount = 1 - (float)_brokenPieces.Count / (float)_initObjNum;
            
            ProgressBarController.Instance.SetBar(amount);
            
            ControlPieces();
        }
    }
}
