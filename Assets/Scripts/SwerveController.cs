using UnityEngine;

public class SwerveController : MonoBehaviour
{
    [SerializeField] private FixedJoystick fixedJoystick;
            
    [Space] 
    [SerializeField] private float speed = 1f;
    [Space] 
    [SerializeField] private float turnSpeed = 5f;
        
    [SerializeField] private float minX, maxX, minZ, maxZ;
        
    private void FixedUpdate()
    {
        if (Input.GetMouseButton(0))
        {
            JoystickV2();
        }
    }

    private void JoystickV2()
    {
        var direction = Vector3.forward * fixedJoystick.Vertical + Vector3.right * fixedJoystick.Horizontal;
        var pos = transform.position + direction * Time.deltaTime * speed;

        pos.x = Mathf.Clamp(pos.x, minX, maxX);
        pos.z = Mathf.Clamp(pos.z, minZ, maxZ);
            
        transform.position = pos;

        if (direction != Vector3.zero)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation,
                Quaternion.LookRotation(direction), turnSpeed * Time.deltaTime);
        }
    }
        
}